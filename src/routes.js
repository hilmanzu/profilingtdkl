import React from 'react';

const Dashboard = React.lazy(() => import('./Dashboard'));

const viewkompetensi = React.lazy(()=> import('./menu/hasil/viewkompetensi'))
const viewuser = React.lazy(()=> import('./menu/hasil/viewuser'))
const viewhasil = React.lazy(()=> import('./menu/hasil/viewhasil'))

const user = React.lazy(()=> import('./menu/pengguna/user'))
const user_nilai = React.lazy(()=> import('./menu/pengguna/user_nilai'))

const keluar = React.lazy(()=> import('./menu/keluar'))

const profile = React.lazy(()=> import('./menu/profile'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/Biodata', name: 'Biodata Saya', component: Dashboard },
  ///report
  { path: '/Viewkompetensi', name: 'Penilai Lapangan', component: viewkompetensi },
  { path: '/Viewuser', name: 'Penilai Lapangan', component: viewuser },
  { path: '/Viewhasil', name: 'User Detail', component: viewhasil },
  ///pengguna
  { path: '/Pengguna', name: 'Pengguna', component: user },
  { path: '/user_nilai', name: 'Penilaian', component: user_nilai },
  ///keluar
  { path: '/Keluar', name: 'Keluar', component: keluar },
  ///profile
  { path: '/Profile', name: 'Profile', component: profile },
];

export default routes;
