import React, { Component } from 'react';
import firebase from 'firebase'
import swal from 'sweetalert';
import ReactTable from 'react-table'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import '../hasil/card.css'
const gambar = "https://centrik.in/wp-content/uploads/2017/02/user-image-.png"

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :''
        }  
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("user")
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member} = this.state;

    return (
      <div class='row'>
        {member.map((data)=>
          <div>
            <Card style={{width:300,height:170,marginLeft:30,marginTop:10}}>
              <div class='row2'>
                <div>
                  <CardImg style={{marginTop:10,marginLeft:10,width:100,height:100}} src= {gambar} alt="Card image cap"/>
                </div>
                <div class='row2' style={{marginTop:10,marginLeft:10,width:300,}}>
                  <div>
                    <CardText>NIP</CardText>
                    <CardText>Nama</CardText>
                    <CardText>Jabatan</CardText>
                  </div>
                  <div>
                    <CardText>: {data.data.nip}</CardText>
                    <CardText>: {data.data.firstname}{data.data.lastname}</CardText>
                    <CardText>: {data.data.jabatan}</CardText>
                  </div>
                </div>
              </div>
              <Button color="success" style={{marginLeft:180,width:100,marginTop:10}}>Beri Nilai</Button>
            </Card>
          </div>
        )}
      </div>
    )
  }

  tambah=()=>{
   this.props.history.push('/edit/tambahmember')
  }

}

export default bonus;
