import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import sygnet from '../../assets/img/brand/sygnet.svg'

const  angkasa = 'https://1.bp.blogspot.com/-mFNOC0eFJ4w/Vv31qj5l5sI/AAAAAAAAAKI/SnAvMQlb4schzKwIONR2SiNRIUqCPQqSg/s1600/Logo_AngkasaPura_2011.png'
const  proxsis = 'https://proxsis.com/wp-content/uploads/2013/05/proxsis1-1.png'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: proxsis, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: "sygnet", width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppNavbarBrand
          full={{ src: angkasa, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: "sygnet", width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
