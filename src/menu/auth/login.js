import React, { Component } from 'react';
import { Link,Route } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert';

const  angkasa = 'https://1.bp.blogspot.com/-mFNOC0eFJ4w/Vv31qj5l5sI/AAAAAAAAAKI/SnAvMQlb4schzKwIONR2SiNRIUqCPQqSg/s1600/Logo_AngkasaPura_2011.png'

class login extends Component {

  constructor(props){
  super(props);
  this.handleChange = this.handleChange.bind(this);
  this.handleChange2 = this.handleChange2.bind(this);
  this.state = {
    nip:'',
    password:'',
    loading: true,
    dashboard:[]
    }
  }

  handleChange(event) {
    this.setState({nip: event.target.value});
  }

  handleChange2(event) {
    this.setState({password: event.target.value});
  }

  componentDidMount(){
    var data = localStorage.getItem('nip');
      if (data){
        this.props.history.push('/')
      }else{
        this.props.history.push('/login')
      }
      const db = firebase.firestore();
      const doc = db.collection("user")
      doc.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
            })
          })
        this.setState({
          dashboard : data
        })
      })

    }

  render() {
    return (
      <div className="app flex-row align-items-center" style={{backgroundImage: "url(" + "https://cdn-u1-gnfi.imgix.net/post/large-1526598981-f538a0953e6af42f23f02bfa11dc294d.jpg" + ")",backgroundPosition: 'center',backgroundSize: 'cover',backgroundRepeat: 'no-repeat'}}>
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="NIP" autoComplete="username" value={this.state.nip} onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" value={this.state.password} onChange={this.handleChange2} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.login}>Login</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }

  login = () => {
    const {dashboard,nip,password} = this.state
    var Nip = dashboard.find((item) => item.data.nip === nip)
  
    if (nip === ''){
      swal('Isi NIP anda')
    }else if (Nip === undefined){
      swal('Akun Tidak Ditemukan')
    }else if (password === ''){
      swal('Isi Password anda')
    }else if (Nip.data.nip === nip && Nip.data.roles[0] === 'EVALUATOR TIDAK LANGSUNG'){
      swal('Selamat Datang')
      localStorage.setItem('nip',Nip.data.nip);
      window.location.reload();
    }else{
      swal('Opss, anda bukan evaluator')
    }

  }

}

export default login
