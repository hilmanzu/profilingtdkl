import React, { Component } from 'react';
import {
  Button,
} from 'reactstrap';
import firebase from 'firebase'
import MUIDataTable from "mui-datatables";

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
          modalIsOpen: false,
          performance : '',
          synergi:'',
          integrity:'',
          bussiness:'',
        }
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("user")
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member} = this.state;
    const list = member.filter((data)=> data.data.status === true)

    const data = Object.values(list).map((data,index)=>
        [ 
          index+1,
          data.data.nip,
          data.data.firstname + data.data.lastname,
          data.data.jabatan,
          data.data.wilayah,
          <div>
            <Button color="success" style={{width:100,marginLeft:20}} onClick={()=>{localStorage.setItem('id',data.id);localStorage.setItem('jabatan',data.data.id_jabatan);localStorage.setItem('model',data.data.id_model);this.props.history.push('/Viewhasil')}}>
              Lihat Data
            </Button>
          </div>
        ]
      )

    const options = {
      filterType: 'dropdown',
      print : false,
      download : false,
      selectableRows : 'none',
      viewColumns : false
    };

    const columns=[
      {
        name: "No",
        label: "No",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "NIP",
        label: "NIP",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "Nama Lengkap",
        label: "Nama",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "Jabatan",
        label: "Jabatan",
        options: {
         filter: true,
         sort: false,
        }
      },{
        name: "Lokasi",
        label: "Lokasi",
        options: {
         filter: true,
         sort: false,
        }
      },{
        name: "Aksi",
        label: "Aksi",
        options: {
         filter: false,
         sort: false,
        }
      }
    ]

    return (
      <div>
        <MUIDataTable
          title={"Pengguna"}
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    )
  }

}

export default bonus;
