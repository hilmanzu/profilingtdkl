import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert';
import renderIf from '../hasil/renderIf'
var ReactTable = require("react-table").default;

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
        }
      }

    componentWillMount(){
      const db = firebase.firestore();
      const docRef = db.collection("user").orderBy('status', 'asc')
        docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        }) 
      })
    }

  render() {
    const {member} = this.state;
    const list = member.filter((data)=> data.data.id_jabatan)
    const columns = [{
                      Header: 'No',
                      id: 'no',
                      width: 40,
                      style: {textAlign: 'center'},
                      Cell: (row) => {return <div>{row.index+1}</div>}
                    },{
                      Header: 'NIP',
                      id: 'nip',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.nip}</p></td>
                        </div>
                    },{
                      Header: 'Nama',
                      id: 'firstname',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p style={{textAlign:'center'}}>{d.data.firstname} {d.data.lastname}</p></td>
                        </div>
                    },{
                      Header: 'Jabatan',
                      id: 'phone',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.jabatan}</p></td>
                        </div>
                    },{
                      id: 'aksi',
                      Header: 'Aksi',
                      width: 150,
                      accessor: d => 

                      <div>
                        {renderIf(d.data.status === true)(
                          <div>
                            <Button color="success" style={{width:100,marginLeft:20}} onClick={()=>{swal('Data Telah diisi','Silahkan lihat di fitur hasil')}}>
                              Selesai
                            </Button>
                          </div>
                        )}
                        {renderIf(d.data.status === false)(
                          <div>
                            <Button color="warning" style={{width:100,marginLeft:20}} onClick={()=>{localStorage.setItem('id_user',d.id);localStorage.setItem('jabatan',d.data.id_jabatan);localStorage.setItem('model',d.data.id_model);this.props.history.push('/user_nilai')}}>
                              Beri Nilai
                            </Button>
                          </div>
                        )}
                      </div>

                    }]

    return (
      <div>
       <ReactTable
          data={list}
          columns={columns}
          defaultPageSize={10}
          className="-striped -highlight"
          noDataText="Data Tidak Ditemukan"
        />
      </div>
    )
  }

}

export default bonus;
