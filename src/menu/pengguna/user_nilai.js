import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert';
import renderIf from '../hasil/renderIf'
import Modal from 'react-modal';
var ReactTable = require("react-table").default;

class user_nilai extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
          modalIsOpen: false,
          performance : '',
          nama:'',
          jabatan:''
        }
        this.penilaian = this.penilaian.bind(this);
      }

    componentWillMount(){
      var jabatan = localStorage.getItem('jabatan');
      var model   = localStorage.getItem('model');
      var id      = localStorage.getItem('id_user');

      const db       = firebase.firestore();
      const user     = db.collection("user").doc(id)
      const jabatans = db.collection("kompetens").doc(model).collection('jabatan').doc(jabatan)
      const docRef   = db.collection("kompetens").doc(model).collection('jabatan').doc(jabatan).collection('cluster').doc('jvJNEf7Ba6DYNCq4W7xh').collection('nama_kompetensi')

      user.onSnapshot( async (doc) => {
          let data = doc.data()
          this.setState({ nama : data })
      })

      jabatans.onSnapshot( async (doc) => {
          let data = doc.data()
          this.setState({ jabatan : data })
      })

      docRef.onSnapshot(async(querySnapshot)=>{
          var data = []
          querySnapshot.forEach((doc)=>{
            let datas = data
            datas.push({
              id   : doc.id,
              data : doc.data()
            })
          })
          this.setState({
            member    : data,
            nomor     : Object.keys(data).length
          })
      })
    }

  render() {
    const {member,nama,jabatan} = this.state;

    return (
          <table>
          <Card style={{paddingLeft:30,paddingRight:20,paddingTop:20,paddingBottom:10}}>
            <div>
              <td>
                <p>Nama User</p>
                <p>NIP</p>
              </td>
              <td>
                <p> :  {nama.firstname} {nama.lastname}</p>
                <p> :  {nama.nip}</p>
              </td>
              <td>
                <p style={{marginLeft:100}}>Lokasi</p>
                <p style={{marginLeft:100}}>Jabatan</p>
              </td>
              <td>
                <p> :  {nama.wilayah}</p>
                <p> :  {jabatan.jabatan}</p>
              </td>
            </div>
            <Card style={{paddingLeft:30,paddingRight:20,paddingTop:20,paddingBottom:10}}>
              <tr>
                <Row style={{marginLeft:305,width:300}}>
                  <p style={{width:50}}>1</p>
                  <p style={{width:50}}>2</p>
                  <p style={{width:50}}>3</p>
                  <p style={{width:50}}>4</p>
                  <p style={{width:50}}>5</p>
                </Row>
              </tr>
              {member.map((data)=>
                <div>
                  <tr>
                      <p style={{width:300}}>{data.data.data_kompetensi}</p>
                      <td style={{width:50}}>
                        <input onChange={this.penilaian} type="radio" name={data.data.data_kompetensi} value="1"/>
                      </td>
                      <td style={{width:50}}>
                        <input onChange={this.penilaian} type="radio" name={data.data.data_kompetensi} value="2"/>
                      </td>
                      <td style={{width:50}}>
                        <input onChange={this.penilaian} type="radio" name={data.data.data_kompetensi} value="3"/>
                      </td>
                      <td style={{width:50}}>
                        <input onChange={this.penilaian} type="radio" name={data.data.data_kompetensi} value="4"/>
                      </td>
                      <td style={{width:50}}>
                        <input onChange={this.penilaian} type="radio" name={data.data.data_kompetensi} value="5"/>
                      </td>
                      <td style={{width:50}}>
                        <input placeholder="ket" style={{width:80}}/>
                      </td>
                  </tr>
                </div>
              )}
              <Button onClick={this.ok} color="success" style={{width:100,marginTop:10,marginBottom:10}}>Selesai</Button>
              </Card>
          </Card>
        </table>
    )
  }

  penilaian(event) {
    const id = localStorage.getItem('id_user');
    const kompetensi = event.target.name
    const nilai = event.target.value
    const db = firebase.firestore()
    const data = db.collection("user").doc(id).collection("Threshold")
    if (kompetensi === event.target.name) {
        data.add({
          nama       : kompetensi,
          nilai      : parseInt(nilai),
          req        : 3
        })
    }

  }

  ok=()=>{
    const db = firebase.firestore()
    const id = localStorage.getItem('id_user')
    const status = db.collection("user").doc(id)
    status.update({
      status       : true
    })
    swal('Nilai telah diisi')
    this.props.history.push('/Pengguna')
  }

}

export default user_nilai;
