import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert';
import renderIf from '../hasil/renderIf'
import Modal from 'react-modal';
var ReactTable = require("react-table").default;

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

const nilai = [{nilai:1,nilai:2,nilai:3,nilai:4,nilai:5}]

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
          modalIsOpen: false,
          performance : '',
          synergi:'',
          integrity:'',
          bussiness:'',
        } 
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this); 
        this.performance = this.performance.bind(this);
        this.synergi = this.synergi.bind(this);
        this.integrity = this.integrity.bind(this);
        this.bussiness = this.bussiness.bind(this);
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("user")
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member,synergi,performance,bussiness,integrity} = this.state;
    console.log(synergi,performance,bussiness,integrity)
    const columns = [{
                      Header: 'No',
                      id: 'no',
                      width: 40,
                      style: {textAlign: 'center'},
                      Cell: (row) => {return <div>{row.index+1}</div>}
                    },{
                      Header: 'NIP',
                      id: 'nip',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.nip}</p></td>
                        </div>
                    },{
                      Header: 'Firstname',
                      id: 'firstname',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.firstname}</p></td>
                        </div>
                    },{
                      Header: 'Lastname',
                      id: 'lastname',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.lastname}</p></td>
                        </div>
                    },{
                      Header: 'Jabatan',
                      id: 'jabatan',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.jabatan}</p></td>
                        </div>
                    },{
                      Header: 'Phone',
                      id: 'phone',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.phone}</p></td>
                        </div>
                    },{
                      id: 'aksi',
                      Header: 'Aksi',
                      width: 150,
                      accessor: d => 

                      <div>
                        {renderIf(d.data.status === true)(
                          <div>
                            <Button color="success" style={{width:100,marginLeft:20}} onClick={()=>{swal('Data Telah diisi')}}>
                              Selesai
                            </Button>
                          </div>
                        )}
                        {renderIf(d.data.status === false)(
                          <div>
                            <Button color="warning" style={{width:100,marginLeft:20}} onClick={this.openModal}>
                              Beri Nilai
                            </Button>
                          </div>
                        )}
                      </div>

                    }]

    return (
      <div>
       <ReactTable
          data={member}
          columns={columns}
          defaultPageSize={10}
          className="-striped -highlight"
          noDataText="Data Tidak Ditemukan"
        />
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <table>
            <tr>
                <th></th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
            </tr>
            <tr>
                <td>Performance</td>
                <td><input value={this.state.performance} onChange={this.performance} type="radio" name="row-1" value="1"/></td>
                <td><input value={this.state.performance} onChange={this.performance} type="radio" name="row-1" value="2"/></td>
                <td><input value={this.state.performance} onChange={this.performance} type="radio" name="row-1" value="3"/></td>
                <td><input value={this.state.performance} onChange={this.performance} type="radio" name="row-1" value="4"/></td>
                <td><input value={this.state.performance} onChange={this.performance} type="radio" name="row-1" value="5"/></td>
            </tr>
            <tr>
                <td>Bussiness</td>
                <td><input value={this.state.bussiness} onChange={this.bussiness} type="radio" name="row-2" value="1"/></td>
                <td><input value={this.state.bussiness} onChange={this.bussiness} type="radio" name="row-2" value="2"/></td>
                <td><input value={this.state.bussiness} onChange={this.bussiness} type="radio" name="row-2" value="3"/></td>
                <td><input value={this.state.bussiness} onChange={this.bussiness} type="radio" name="row-2" value="4"/></td>
                <td><input value={this.state.bussiness} onChange={this.bussiness} type="radio" name="row-2" value="5"/></td>
            </tr>
            <tr>
                <td>Integrity</td>
                <td><input value={this.state.integrity} onChange={this.integrity} type="radio" name="row-3" value="1"/></td>
                <td><input value={this.state.integrity} onChange={this.integrity} type="radio" name="row-3" value="2"/></td>
                <td><input value={this.state.integrity} onChange={this.integrity} type="radio" name="row-3" value="3"/></td>
                <td><input value={this.state.integrity} onChange={this.integrity} type="radio" name="row-3" value="4"/></td>
                <td><input value={this.state.integrity} onChange={this.integrity} type="radio" name="row-3" value="5"/></td>
            </tr>
            <tr>
                <td>Synergi</td>
                <td><input value={this.state.synergi} onChange={this.synergi} type="radio" name="row-4" value="1"/></td>
                <td><input value={this.state.synergi} onChange={this.synergi} type="radio" name="row-4" value="2"/></td>
                <td><input value={this.state.synergi} onChange={this.synergi} type="radio" name="row-4" value="3"/></td>
                <td><input value={this.state.synergi} onChange={this.synergi} type="radio" name="row-4" value="4"/></td>
                <td><input value={this.state.synergi} onChange={this.synergi} type="radio" name="row-4" value="5"/></td>
            </tr>
          </table>

          
        </Modal>
      </div>
    )
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }

  performance(event) {
    this.setState({performance: event.target.value});
  } 

  synergi(event) {
    this.setState({synergi: event.target.value});
  } 

  integrity(event) {
    this.setState({integrity: event.target.value});
  } 

  bussiness(event) {
    this.setState({bussiness: event.target.value});
  } 

}

export default bonus;
