export default {
  items: [
    {
      name: 'Biodata Saya',
      icon: 'icon-user',
    },
    {
      name: 'Hasil',
      url: '/Viewkompetensi',
      icon: 'icon-docs',
    },
    {
      name: 'Keluar',
      url: '/Keluar',
      icon: 'icon-logout',
    }
  ],
};
