import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import './App.scss';
import firebase from 'firebase'

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = Loadable({
  loader: () => import('./containers/DefaultLayout'),
  loading
});

// Pages
const Login = Loadable({
  loader: () => import('./menu/auth/login'),
  loading
});

class App extends Component {

  render() {
    return (
      <HashRouter>
          <Switch>
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route path="/" name="Home" component={DefaultLayout} />
          </Switch>
      </HashRouter>
    );
  }
}
            // <Route exact path="/register" name="Register Page" component={Register} />

export default App;

var config = {
    apiKey: "AIzaSyASWOqUppmRp0XFVb8uw8RTHbiVnJ6E_xY",
    authDomain: "asesmen-hr.firebaseapp.com",
    databaseURL: "https://asesmen-hr.firebaseio.com",
    projectId: "asesmen-hr",
    storageBucket: "asesmen-hr.appspot.com",
    messagingSenderId: "661722095498",
    appId: "1:661722095498:web:d356510d04dc04c8"
  };
  firebase.initializeApp(config);
